﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototipos.DB.Pedido
{
    class PedidoDTO
    {
        public int Id_Pedido { get; set; }
        public string Ed_Endereco { get; set; }
        public int Cliente_id_cliente { get; set; }
    }
}

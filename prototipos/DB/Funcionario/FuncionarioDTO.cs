﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prototipos.DB.Funcionario
{
    class FuncionarioDTO
    {
        public int Id_Funcionario { get; set; }
        public string Nm_Nome { get; set; }
        public string Loguin { get; set; }
        public string Senha { get; set; }
    }
}
